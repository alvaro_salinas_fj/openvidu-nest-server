# openvidu management server

If you want to run these application make sure that your environment variables are in order.

## Docker dependency

Please ake sure to run the following command before running the openvidu-management-server

```console
docker run -p 4443:4443 --rm \
    -e OPENVIDU_SECRET=MY_SECRET \
    -e OPENVIDU_RECORDING=true \
    -e OPENVIDU_RECORDING_PATH=/opt/openvidu/recordings \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /Users/gordex/Desktop/recordings:/opt/openvidu/recordings \
openvidu/openvidu-server-kms:2.20.0
```