import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { SessionController } from './session.controller';
import { SessionRepository } from './session.repository';
import { SessionService } from './session.service';

@Module({
  controllers: [SessionController],
  exports: [SessionService],
  imports: [CommonModule],
  providers: [SessionService, SessionRepository],
})
export class SessionModule {}
