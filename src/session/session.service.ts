import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import { Session } from 'openvidu-node-client';
import { OpenViduService } from 'src/common/open-vidu/open-vidu.service';
import { SessionJSON } from 'src/common/utils/utils.entities';
import { UtilsService } from 'src/common/utils/utils.service';
import { SessionsMap } from './session.entities';
import { SessionRepository } from './session.repository';

@Injectable()
export class SessionService {
  private readonly _sessionRepository: SessionRepository;
  private readonly _openViduService: OpenViduService;
  private readonly _utilsService: UtilsService;

  constructor(
    sessionRepository: SessionRepository,
    openViduService: OpenViduService,
    utilsService: UtilsService,
  ) {
    this._sessionRepository = sessionRepository;
    this._openViduService = openViduService;
    this._utilsService = utilsService;
  }

  /**
   * Creates a new session room, if the session already exists, it returns an exception
   * @param sessionName is the name of the session room to create
   */
  async createSession(sessionName: string): Promise<void> {
    const targetSession = this.getSessionByName(sessionName);
    if (!targetSession) {
      const newSessions = await this._openViduService.createSession();
      this._sessionRepository.createSession(sessionName, newSessions);
    } else {
      throw new BadRequestException(`Session ${sessionName} already exists`);
    }
  }

  /**
   * the map of the active sessions, where the key is the session name and the value
   * is the session object
   * @returns The sessionMap object
   */
  getSessions(): SessionsMap {
    return this._sessionRepository.getSessions();
  }

  /**
   * Tries to retrieve the active sessions in a JSON format directly from the OpenVidu Server
   * @returns the active sessions in a readable format
   */
  async getActiveSessionsJSON(): Promise<SessionJSON[]> {
    const response = await this._openViduService.getActiveSessionsJSON();
    return response;
  }

  /**
   * Tries to return a session in a human readable format
   * @param sessionName is the name of the session room
   * @returns the session in a readable format
   */
  getSessionJsonByName(sessionName: string): SessionJSON {
    const session = this._sessionRepository.getSessionByName(sessionName);

    const sessionJSON = this._utilsService.sessionToJson(session);

    return sessionJSON;
  }

  /**
   * Tries to retrieve a session by its name
   * @param sessionName is the name of the session room to get
   * @returns the session object if it exists, undefined otherwise
   */
  getSessionByName(sessionName: string): Session | undefined {
    return this._sessionRepository.getSessionByName(sessionName);
  }

  /**
   * Retrieve a session by its name, if not found, it returns a 404 exception
   * @param sessionName is the name of the session room to get
   * @returns the session object if it exists, an exception otherwise
   */
  getSessionByNameWithThrowableException(sessionName: string): Session {
    const session = this._sessionRepository.getSessionByName(sessionName);
    if (!session) {
      throw new BadRequestException(`Session ${sessionName} does not exist`);
    }

    return session;
  }

  /**
   * Tries to retrieve the session id of a session by its name, if the session
   * does not exist, it returns a 404 exception
   * @param sessionName is the name of the session room
   * @returns The session id of the session room
   */
  getSessionIdBySessionName(sessionName: string): string {
    const session = this.getSessionByName(sessionName);

    if (!session) {
      throw new BadRequestException(`Session ${sessionName} does not exist`);
    }

    const sessionId = session.sessionId;
    return sessionId;
  }

  /**
   * Tries to update the session by its name, if the session does not exist, it returns a
   * 404 exception
   * @param sessionName is the name of the session room
   * @param session is the session to be updated
   * @returns void if the session exists, an exception otherwise
   */
  updateSessionBySessionName(sessionName: string, session: Session) {
    const targetSession =
      this.getSessionByNameWithThrowableException(sessionName);

    if (!targetSession) {
      throw new BadRequestException(`Session ${sessionName} does not exist`);
    }

    this._sessionRepository.updateSessionBySessionName(sessionName, session);
  }

  /**
   * Tries to close a session, clean all the publishers and its respective subscribers
   * @param sessionName is the name of the session room
   */
  async closeSessionGracefully(sessionName: string): Promise<void> {
    const session = this.getSessionByNameWithThrowableException(sessionName);

    try {
      await session.close();
      this._sessionRepository.deleteSessionBySessionName(sessionName);
    } catch (error) {
      throw new ConflictException('Session could not be closed');
    }
  }

  /**
   * @param sessionName is the name of the session room to delete
   */
  deleteSessionBySessionName(sessionName: string): void {
    this._sessionRepository.deleteSessionBySessionName(sessionName);
  }

  /**
   * Tries to start the recording process for an entire session
   * @param sessionName is the name of the session room
   * @param filename Is the name of the file to be saved
   * @returns the recording id as string
   */
  async startSessionRecording(sessionName: string, filename: string) {
    const sessionId = this.getSessionIdBySessionName(sessionName);

    return await this._openViduService.startRecording(sessionId, filename);
  }

  /**
   * Tries to stop the recording process for an entire session
   * @param recordingId is the id of the recording to stop
   */
  async stopSessionRecording(recordingId: string) {
    await this._openViduService.stopRecording(recordingId);
  }

  addTokenToSessionBySessionName(sessionName: string, token: string) {
    this._sessionRepository.addTokenToSessionBySessionName(sessionName, token);
  }

  getTokensBySessionName(sessionName: string) {
    return this._sessionRepository.getTokensBySessionName(sessionName);
  }

  deleteTokenFromTokensMapBySessionName(sessionName: string, token: string) {
    this._sessionRepository.deleteTokenFromTokensMapBySessionName(
      sessionName,
      token,
    );
  }
}
