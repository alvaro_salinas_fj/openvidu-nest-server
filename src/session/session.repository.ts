import { Injectable, NotFoundException } from '@nestjs/common';
import { Session } from 'openvidu-node-client';
import _ from 'lodash';

import {
  RecordingIdByFileName,
  RecordingsIdsBySessionNameMap,
  SessionsMap,
  SessionsNameTokensMap,
} from './session.entities';

@Injectable()
export class SessionRepository {
  private _sessionMap: SessionsMap = {};
  private _sessionsNameTokensMap: SessionsNameTokensMap = {};
  private _recordingsIdsBySessionMap: RecordingsIdsBySessionNameMap = {};

  /**
   * Adds the first session to the session map, where sessionName is the key corresponding
   * to the session and also maps the sessionName to the an empty array of session's tokens
   * @param sessionName is the name of the session
   * @param session is the session object
   */
  createSession(sessionName: string, session: Session) {
    this._sessionMap[sessionName] = session;
    this._sessionsNameTokensMap[sessionName] = [];
  }

  /**
   * @returns the map of all active sessions in a non readable format
   */
  getSessions() {
    return this._sessionMap;
  }

  /**
   * Retrieves the session by session name, if nothings found it returns undefined
   * @param sessionName is the name of the session
   * @returns the session object, undefined if not found
   */
  getSessionByName(sessionName: string): Session | undefined {
    return this._sessionMap[sessionName];
  }

  /**
   * Tries to update a session by the sessionName, if the sessionName does not exist it throws
   * an 404 error
   * @param sessionName is the name of the session
   * @param session is the updated session object
   * @returns void if success, NotFoundException if sessionName does not exists
   */
  updateSessionBySessionName(sessionName: string, session: Session) {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    this._sessionMap[sessionName] = session;
  }

  /**
   * Deletes a session from the session map by its name and the tokens from the tokens map
   * corresponding to the sessionName
   * @param sessionName is the name of the session to be deleted
   * @returns void if the session is deleted, otherwise throws an error
   */
  deleteSessionBySessionName(sessionName: string): void | NotFoundException {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    delete this._sessionMap[sessionName];
    delete this._sessionsNameTokensMap[sessionName];
  }

  /**
   * Creates an object with the name as filename and the recording id, then push the object
   * to the recordingsIds array of a corresponding sessionName, the structure is the following
   * aRandomSessionName: recordingsIdsArray
   * @param sessionName is the name of the session
   * @param fileName is the name of the file
   * @param recordingId is the unique recording id
   */
  addRecordingIdToRecordingsIdsMap(
    sessionName: string,
    fileName: string,
    recordingId: string,
  ): void | NotFoundException {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    const recordingIdByName: RecordingIdByFileName = {};
    recordingIdByName[fileName] = recordingId;

    this._recordingsIdsBySessionMap[sessionName].push(recordingIdByName);
  }

  /**
   * Deletes a recording id from the recordingsIds array of a corresponding sessionName by the
   * previously saved fileName
   * @param sessionName is the name of the session
   * @param fileName is the name of the file which is the key to find the recording id
   */
  deleteRecordingIdFromRecordingsIdsBySessionNameMap(
    sessionName: string,
    fileName: string,
  ) {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    const index = _.findIndex(
      this._recordingsIdsBySessionMap[sessionName],
      (recording) => {
        return recording[fileName] === fileName;
      },
    );

    if (index === -1) {
      return new NotFoundException(`File name ${fileName} does not exits`);
    } else {
      this._recordingsIdsBySessionMap[sessionName].splice(index, 1);
    }
  }

  /**
   * Returns the recordingId by the sessionName and the fileName previously chosen
   * @param sessionName is the name of the session
   * @param fileName is the name of the saved file, which is the key to find the recording id
   * @returns the recording id as string
   */
  getRecordingIdByFileName(sessionName: string, fileName: string) {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`Session ${sessionName} does not exits`);
    }

    const recordingIdByName: RecordingIdByFileName = _.find(
      this._recordingsIdsBySessionMap[sessionName],
      (recording) => {
        return recording[fileName] === fileName;
      },
    );

    if (!recordingIdByName) {
      return new NotFoundException(
        `RecordingId for ${fileName} does not exits`,
      );
    }

    return recordingIdByName[fileName];
  }

  /**
   * Tries to add a token to the tokens array of a corresponding sessionName, if the sessionName
   * does not exist it throws an 404 error
   * @param sessionName is the name of the session
   * @param token is the token as string
   * @returns void if success or a NotFoundException
   */
  addTokenToSessionBySessionName(
    sessionName: string,
    token: string,
  ): void | NotFoundException {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    this._sessionsNameTokensMap[sessionName].push(token);
  }

  /**
   * Tries to retrieve the tokens array of a corresponding sessionName, if the sessionName
   * does not exist it throws an 404 error
   * @param sessionName is the name of the session
   * @returns the tokens array of a corresponding sessionName, 404 otherwise
   */
  getTokensBySessionName(sessionName: string): string[] | NotFoundException {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    return this._sessionsNameTokensMap[sessionName];
  }

  /**
   * Tries to delete a token from the tokens array of a corresponding sessionName in
   * tokensMap object, if the sessionName does not exist it throws an 404 error
   * @param sessionName is the name of the session
   * @param token is the token value as string
   * @returns void if success, NotFoundException otherwise
   */
  deleteTokenFromTokensMapBySessionName(sessionName: string, token: string) {
    if (!this._sessionMap[sessionName]) {
      return new NotFoundException(`${sessionName} does not exits`);
    }

    const index = this._sessionsNameTokensMap[sessionName].indexOf(token);
    if (index === -1) {
      return new NotFoundException(`Token ${token} does not exits`);
    } else {
      this._sessionsNameTokensMap[sessionName].splice(index, 1);
    }
  }
}
