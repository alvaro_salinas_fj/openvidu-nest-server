import { OpenViduRole, Session } from 'openvidu-node-client';

export class User {
  user: string;
  role: OpenViduRole;
}

export class SessionsMap {
  [key: string]: Session;
}

export class SessionsNameTokensMap {
  [key: string]: string[];
}

export class RecordingsIdsBySessionNameMap {
  [key: string]: RecordingIdByFileName[];
}

export class RecordingIdByFileName {
  [key: string]: string;
}
