import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { CreateSessionDto } from './dtos/session.dto';
import { SessionService } from './session.service';

@Controller('v1.0/sessions')
export class SessionController {
  private readonly _sessionService: SessionService;

  constructor(sessionService: SessionService) {
    this._sessionService = sessionService;
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async createSession(@Body() payload: CreateSessionDto) {
    const { sessionName } = payload;
    await this._sessionService.createSession(sessionName);
  }

  @Delete(':sessionName/close')
  @HttpCode(HttpStatus.NO_CONTENT)
  async closeSessionGracefully(@Param('sessionName') sessionName: string) {
    await this._sessionService.closeSessionGracefully(sessionName);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  async getActiveSessions() {
    return await this._sessionService.getActiveSessionsJSON();
  }

  @Get('sessionId/:sessionName')
  @HttpCode(HttpStatus.OK)
  async getSessionId(@Param('sessionName') sessionName: string) {
    const sessionId =
      this._sessionService.getSessionIdBySessionName(sessionName);
    return { sessionId };
  }

  @Get(':sessionName')
  @HttpCode(HttpStatus.OK)
  async getSessionJsonByName(@Param('sessionName') sessionName: string) {
    const sessionId = this._sessionService.getSessionJsonByName(sessionName);
    return { sessionId };
  }

  @Post(':sessionName/start-recording')
  @HttpCode(HttpStatus.CREATED)
  async startRecording(
    @Param('sessionName') sessionName: string,
    @Body('filename') filename: string,
  ) {
    const RecordingId = await this._sessionService.startSessionRecording(
      sessionName,
      filename,
    );

    return { RecordingId };
  }

  @Post(':recordingId/stop-recording')
  @HttpCode(HttpStatus.ACCEPTED)
  async stopRecording(@Param('recordingId') recordingId: string) {
    await this._sessionService.stopSessionRecording(recordingId);
  }
}
