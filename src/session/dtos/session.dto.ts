import { IsNotEmpty, IsString } from 'class-validator';

export class CreateSessionDto {
  @IsString()
  @IsNotEmpty()
  readonly sessionName: string;
}

export class CloseSessionGracefullyDto {
  @IsString()
  @IsNotEmpty()
  readonly sessionName: string;
}
