import { OpenViduRole } from 'openvidu-node-client';

export class ConnectionProperties {
  role: OpenViduRole;
  data: string;
  record: boolean;
}
