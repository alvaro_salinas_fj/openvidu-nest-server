import {
  IsString,
  IsBoolean,
  IsNotEmpty,
  IsEnum,
  IsOptional,
} from 'class-validator';

import { OpenViduRole } from 'openvidu-node-client';

export class CreateConnectionDto {
  @IsEnum(OpenViduRole)
  @IsOptional()
  readonly role: OpenViduRole = OpenViduRole.PUBLISHER;

  @IsBoolean()
  @IsOptional()
  readonly record: boolean = true;

  @IsString()
  @IsNotEmpty()
  readonly data: string;

  @IsString()
  @IsNotEmpty()
  readonly sessionName: string;
}

export class ForceDisconnectDto {
  @IsString()
  @IsNotEmpty()
  readonly sessionName: string;

  @IsString()
  @IsNotEmpty()
  readonly connectionId: string;
}
