import { Module } from '@nestjs/common';
import { CommonModule } from 'src/common/common.module';
import { SessionModule } from 'src/session/session.module';
import { ConnectionController } from './connection.controller';
import { ConnectionService } from './connection.service';

@Module({
  controllers: [ConnectionController],
  imports: [CommonModule, SessionModule],
  providers: [ConnectionService],
})
export class ConnectionModule {}
