import { Injectable, NotFoundException } from '@nestjs/common';
import { ConnectionProperties } from 'openvidu-node-client';
import { SessionService } from 'src/session/session.service';

@Injectable()
export class ConnectionService {
  private readonly _sessionService: SessionService;

  constructor(sessionService: SessionService) {
    this._sessionService = sessionService;
  }

  /**
   * Creates a new connection and attaches these connection into an existing session,
   * returns the connection token and the connection id
   * @param connectionProperties Might include OpenVidu role and the recording options
   * @param sessionName Is the name of the session
   * @returns an object with a token and the connectionId
   */
  async createConnection(
    connectionProperties: ConnectionProperties,
    sessionName: string,
  ): Promise<{ token: string; connectionId: string }> {
    try {
      const session =
        this._sessionService.getSessionByNameWithThrowableException(
          sessionName,
        );

      await session.fetch();

      const { token, connectionId } = await session.createConnection(
        connectionProperties,
      );

      return {
        token,
        connectionId,
      };
    } catch (error) {
      throw new NotFoundException(`Session ${sessionName} doesn't exist`);
    }
  }

  /**
   * Closes a connection for a session by its connectionId
   * @param sessionName is the name of the rooms session
   * @param connectionId is the unique identifier of the connection
   */
  async closeConnection(
    sessionName: string,
    connectionId: string,
  ): Promise<void> {
    try {
      const session =
        this._sessionService.getSessionByNameWithThrowableException(
          sessionName,
        );
      await session.forceDisconnect(connectionId);
    } catch (error) {
      throw new NotFoundException(`Connection ${connectionId} not found`);
    }
  }
}
