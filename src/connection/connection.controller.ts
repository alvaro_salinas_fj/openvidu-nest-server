import {
  Body,
  Controller,
  Delete,
  HttpCode,
  HttpStatus,
  Param,
  Post,
} from '@nestjs/common';
import { ConnectionProperties } from 'openvidu-node-client';
import { ConnectionService } from './connection.service';
import { CreateConnectionDto } from './dtos/connection.dto';

@Controller('v1.0/connections')
export class ConnectionController {
  private readonly _connectionService: ConnectionService;

  constructor(connectionService: ConnectionService) {
    this._connectionService = connectionService;
  }

  @Post('create')
  @HttpCode(HttpStatus.CREATED)
  async createConnection(@Body() payload: CreateConnectionDto) {
    const { sessionName, ...rest } = payload;
    const connectionProperties: ConnectionProperties = { ...rest };

    const connectionCredentials =
      await this._connectionService.createConnection(
        connectionProperties,
        sessionName,
      );

    return { ...connectionCredentials };
  }

  @Delete(':connectionId/session/:sessionName/disconnect')
  @HttpCode(HttpStatus.NO_CONTENT)
  async disconnect(
    @Param('sessionName') sessionName: string,
    @Param('connectionId') connectionId: string,
  ) {
    await this._connectionService.closeConnection(sessionName, connectionId);
  }
}
