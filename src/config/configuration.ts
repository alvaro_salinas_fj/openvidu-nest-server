export default () => ({
  port: parseInt(process.env.PORT, 10) || 3003,
  openvidu: {
    secret: process.env.OPENVIDU_SECRET || 'MY_SECRET',
    url: process.env.OPENVIDU_URL || 'https://localhost:4443',
  },
});
