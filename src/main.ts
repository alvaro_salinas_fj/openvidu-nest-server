import * as fs from 'fs';
import { NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';

const crPath = './certs/openviducert.pem';
const pkPath = './certs/openvidukey.pem';
const options: any = {};

if (fs.existsSync(crPath) && fs.existsSync(pkPath)) {
  options.httpsOptions = {
    cert: fs.readFileSync(crPath),
    key: fs.readFileSync(pkPath),
  };
}

async function bootstrap() {
  const logger = new Logger('Server');

  const app = await NestFactory.create(AppModule, options);
  app.enableCors();
  const configureService: ConfigService = app.get<ConfigService>(ConfigService);
  // app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );
  const port = configureService.get<number>('port');

  await app.listen(port, () => logger.log(`Running on port ${port}.`));
}
bootstrap();
