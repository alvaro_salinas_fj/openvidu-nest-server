import { Injectable } from '@nestjs/common';
import { Publisher, Connection, Session } from 'openvidu-node-client';

import {
  ActiveConnectionsForSession,
  ConnectionJSON,
  PublisherJSON,
  SessionJSON,
} from './utils.entities';

@Injectable()
export class UtilsService {
  sessionToJson(session: Session): SessionJSON {
    const sessionJSON: SessionJSON = {};

    sessionJSON.sessionId = session.sessionId;
    sessionJSON.createdAt = session.createdAt;
    sessionJSON.customSessionId = !!session.properties.customSessionId
      ? session.properties.customSessionId
      : '';
    sessionJSON.recording = session.recording;
    sessionJSON.mediaMode = session.properties.mediaMode;
    sessionJSON.recordingMode = session.properties.recordingMode;
    sessionJSON.defaultRecordingProperties =
      session.properties.defaultRecordingProperties;

    const connections: ActiveConnectionsForSession = {};
    connections.numberOfElements = session.activeConnections.length;
    connections.content = this._connectionsArrayBuilder(session.connections);
    sessionJSON.connections = connections;

    return sessionJSON;
  }

  private _connectionsArrayBuilder(
    activeConnections: Connection[],
  ): ConnectionJSON[] {
    const connectionsArray: ConnectionJSON[] = [];

    activeConnections.forEach((connection) => {
      const connectionData: ConnectionJSON = {};
      connectionData.connectionId = connection.connectionId;
      connectionData.createdAt = connection.createdAt;
      connectionData.role = connection.role;
      connectionData.serverData = connection.serverData;
      // connectionToDeliver.record = connection.record;
      connectionData.token = connection.token;
      connectionData.clientData = connection.clientData;

      const subscribers: string[] = [];
      connection.subscribers.forEach((sub) => {
        subscribers.push(sub);
      });
      connectionData.subscribers = subscribers;
      connectionData.publishers = this._publishersArrayBuilder(
        connection.publishers,
      );
      connectionsArray.push(connectionData);
    });

    return connectionsArray;
  }

  private _publishersArrayBuilder(
    connectionPublishers: Publisher[],
  ): PublisherJSON[] {
    const publishers: PublisherJSON[] = [];

    connectionPublishers.forEach((publisher) => {
      const publisherJSON: PublisherJSON = {};
      publisherJSON.streamId = publisher.streamId;
      publisherJSON.createdAt = publisher.createdAt;
      publisherJSON.hasAudio = publisher.hasAudio;
      publisherJSON.hasVideo = publisher.hasVideo;
      publisherJSON.audioActive = publisher.audioActive;
      publisherJSON.videoActive = publisher.videoActive;
      publisherJSON.frameRate = publisher.frameRate;
      publisherJSON.typeOfVideo = publisher.typeOfVideo;
      publisherJSON.videoDimensions = publisher.videoDimensions;
      publishers.push(publisherJSON);
    });

    return publishers;
  }
}
