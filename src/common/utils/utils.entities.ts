import {
  MediaMode,
  OpenViduRole,
  RecordingMode,
  RecordingProperties,
} from 'openvidu-node-client';

export class SessionJSON {
  sessionId?: string;
  createdAt?: number;
  customSessionId?: string | undefined;
  recording?: boolean;
  mediaMode?: MediaMode | undefined;
  recordingMode?: RecordingMode | undefined;
  defaultRecordingProperties?: RecordingProperties | undefined;
  connections?: ActiveConnectionsForSession;
}

export class ConnectionJSON {
  connectionId?: string;
  createdAt?: number;
  role?: OpenViduRole | undefined;
  serverData?: string | undefined;
  token?: string;
  clientData?: string;
  subscribers?: string[];
  publishers?: PublisherJSON[];
}

export class PublisherJSON {
  streamId?: string;
  createdAt?: number;
  hasAudio?: boolean;
  hasVideo?: boolean;
  audioActive?: boolean;
  videoActive?: boolean;
  frameRate?: number;
  typeOfVideo?: string;
  videoDimensions?: string;
}

export class ActiveConnectionsForSession {
  numberOfElements?: number;
  content?: ConnectionJSON[];
}
