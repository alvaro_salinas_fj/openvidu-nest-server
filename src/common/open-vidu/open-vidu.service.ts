import {
  ConflictException,
  Injectable,
  NotAcceptableException,
  NotFoundException,
  NotImplementedException,
  ServiceUnavailableException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  SessionProperties,
  OpenVidu,
  RecordingMode,
  Recording,
  Session,
} from 'openvidu-node-client';
import { SessionJSON } from '../utils/utils.entities';
import { UtilsService } from '../utils/utils.service';

@Injectable()
export class OpenViduService {
  private readonly _configService: ConfigService;
  private readonly _openviduUrl: string;
  private readonly _openviduSecret: string;
  private readonly _OV: OpenVidu;
  private readonly _utilsService: UtilsService;

  constructor(configService: ConfigService, utilsService: UtilsService) {
    this._configService = configService;
    this._openviduUrl = this._configService.get<string>('openvidu.url');
    this._openviduSecret = this._configService.get<string>('openvidu.secret');
    this._OV = new OpenVidu(this._openviduUrl, this._openviduSecret);
    this._utilsService = utilsService;
  }

  /**
   * Creates a new session pre-configured to save streams manually
   * @returns A new session object
   */
  async createSession(): Promise<Session> {
    const sessionProperties: SessionProperties = {
      recordingMode: RecordingMode.MANUAL,
      defaultRecordingProperties: {
        outputMode: Recording.OutputMode.INDIVIDUAL,
        resolution: '640x480',
        frameRate: 15,
      },
    };

    try {
      const newSession = await this._OV.createSession(sessionProperties);
      return newSession;
    } catch (error) {
      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * Starts the recording of a session (sessionId) and saves the result in a file (fileName)
   * @returns recordingId which is the unique identifier of the recording
   * @param sessionId is the session id to be passed to the OpenVidu Server
   * @param fileName is the filename to be saved
   */
  async startRecording(sessionId: string, fileName: string): Promise<string> {
    const now = Date.now();
    // TODO: use uuid
    const uniqueFileName = `${fileName}-${now}`;
    console.log(sessionId, uniqueFileName);

    try {
      const recording = await this._OV.startRecording(
        sessionId,
        uniqueFileName,
      );

      return recording.id;
    } catch (error) {
      console.log(error);
      if (error.status === 404) {
        throw new NotFoundException(`Session ${sessionId} not found`);
      } else if (error.status === 406) {
        throw new NotAcceptableException(
          'The session has no connected participants',
        );
      } else if (error.status === 422) {
        throw new UnprocessableEntityException(
          'When passing "RecordingProperties", resolution parameter exceeds acceptable values (for both width and height, min 100px and max 1999px) or trying to start a recording with both hasAudio and hasVideo to false',
        );
      } else if (error.status === 409) {
        throw new ConflictException(
          'the session is not configured for using MediaMode.ROUTED or it is already being recorded',
        );
      } else if (error.status === 501) {
        throw new NotImplementedException(
          'OpenVidu Server recording module is disabled (OPENVIDU_RECORDING property set to false)',
        );
      }

      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * Stops the recording of a started recording
   * @param recordingId is the unique identifier of the recording
   */
  async stopRecording(recordingId: string): Promise<void> {
    try {
      await this._OV.stopRecording(recordingId);
    } catch (error) {
      if (error.status === 404) {
        throw new NotFoundException(`Recording ${recordingId} not found`);
      } else if (error.status === 406) {
        throw new NotAcceptableException(
          `The recording ${recordingId} has a "starting" status. Wait until "started" status before stopping the recording`,
        );
      }

      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * List all the recordings objects from the OpenVidu Server in an array
   * @returns An array of recording objects
   */
  async listRecordings(): Promise<Recording[]> {
    try {
      const recordings = await this._OV.listRecordings();
      return recordings;
    } catch (error) {
      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * List all the recordings ids from the OpenVidu Server in an array
   * @returns An array of recording ids
   */
  async listRecordingsIds(): Promise<string[]> {
    const recordingsIds: string[] = [];
    try {
      const recordings = await this._OV.listRecordings();

      recordings.forEach((recording) => {
        recordingsIds.push(recording.id);
      });

      return recordingsIds;
    } catch (error) {
      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * Retrieves the recording object from the OpenVidu Server
   * @param recordingId is the unique identifier of the recording
   * @returns A promise with the recording object
   */
  async getRecording(recordingId: string): Promise<Recording> {
    try {
      const recording = await this._OV.getRecording(recordingId);
      return recording;
    } catch (error) {
      if (error.status === 404) {
        throw new NotFoundException(`Recording ${recordingId} not found`);
      }
      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }

  /**
   * Retrieves all the sessions information in a readable format
   * @returns An object with all of active sessions to be passed to the frontend
   */
  async getActiveSessionsJSON(): Promise<SessionJSON[]> {
    const sessionsInfo: SessionJSON[] = [];
    try {
      this._OV.activeSessions.forEach((session) => {
        sessionsInfo.push(this._utilsService.sessionToJson(session));
      });

      return sessionsInfo;
    } catch (error) {
      throw new ServiceUnavailableException('OpenVidu server unreachable');
    }
  }
}
