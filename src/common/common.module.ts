import { Module } from '@nestjs/common';
import { OpenViduService } from './open-vidu/open-vidu.service';
import { UtilsService } from './utils/utils.service';

@Module({
  providers: [OpenViduService, UtilsService],
  exports: [OpenViduService, UtilsService],
})
export class CommonModule {}
